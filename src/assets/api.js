import Axios from "axios";
import Swal from "sweetalert2";
// http://54.71.136.125:3000/respuesta/
// http://localhost:3000/respuesta/
// http://cloud.lucasian.com:3000/respuesta/
export function guardarRespuesta(id_Encuesta, respuesta, i, cant) {
    const url = 'http://cloud.lucasian.com:3000/respuesta/' + id_Encuesta;
    Axios.post(url, respuesta)
        .then(() => {
            if ((i + 1) === cant) {

                Swal.fire({
                    title: 'Muchas gracias!!!!',
                    text: 'La encuesta se ha completado satisfactoriamente',
                    icon: 'success'
                }).then(() => {
                    window.location.href = "https://www.lucasian.com/"
                });
            }

        }).catch(() => {

            Swal.fire('Error', 'Su encuesta no se ha podido enviar', 'error');
        });
}